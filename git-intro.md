# Introduction to git

git is a command line tool for version control of text based files. It succeeds popular technologies that were popular before, such as [cvs](https://en.wikipedia.org/wiki/Concurrent_Versions_System) and [svn](https://en.wikipedia.org/wiki/Apache_Subversion).

Although there are popular services that offer graphical interface for using git, they all use the git command line tool underneath. No matter what tool you use, you always have the git command line as "last resort" at hand. Therefore, it is useful to be familiar with the most common commands:

## Most basic git commands

These are the most common git commands. A complete reference list can be found at https://git-scm.com/docs . If you like to study git in full depth, you might want to start with the ebook [Pro Git, available at git-scm.com](https://git-scm.com/book/en/v2)

### git clone

`git clone <url_to_other_git_repository>` is the start of every local development: it creates a deep copy of the referenced repository (called *origin* repository). After running this comman, you have the repository and all its history on your computer.

Reference: https://git-scm.com/docs/git-clone

### git show 
`git show` lists all changed files in your repository. If you like to review changes in detail, you may call `git diff` to see all changed lines.

Reference: https://git-scm.com/docs/git-show
Reference: https://git-scm.com/docs/git-diff

### git add
`git add <path>` *stages* files. All staged files are ready for commit.

Reference: https://git-scm.com/docs/git-add

### git commit -m
`git commit -m <commit message>` stores all staged changes in your *local* git repository. From now on your local repository is 1 commit ahead of the origin repository.

Reference: https://git-scm.com/docs/git-commit

### git push
`git push` sends your complete local repository to *origin*. **This command is rejected, when origin has changed that are missing in your local repository**.

Refernce: https://git-scm.com/docs/git-push

### git pull
`git pull` gets all new changes from origin and adds them to your local repository. **Only when you have all changes from origin in your local repository, you may call `git push`**

Reference: https://git-scm.com/docs/git-pull

### git branch
`git branch <branch_name>` creates a new branch from the current local repository.

Reference: https://git-scm.com/docs/git-branch

## git tools

Especially when it comes to merge conflicts, you might want you use tools with graphical user interfaces. Popular tools are: https://git-scm.com/downloads/guis/

**Never manage a repository with more than 1 tool at a time**. Tools might interfere each others view on the repository. Especially GitKraken has an own implementation of git that might lead to broken local repository when opened with another git tool at the same time.

