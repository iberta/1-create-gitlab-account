# Gitlab Pipeline

Gitlab does not only provide git support but is a complete software development platform including a ci. Although mighty it is very easy to learn you can see quick and powerful results very soon.

Reference: [CI/CD in Gitlab](https://docs.gitlab.com/ee/ci/README.html)
Reference: [Pipeline manual for Gitlab](https://docs.gitlab.com/ee/ci/yaml/README.html)

## Task: Create your first pipeline
* [ ]  Create `.gitlab.yml` file in this branch with the following content:

```yml
stages:
  - build
  - deploy
  
Build my demo:
  stage: build
  script:
    - sleep 10s
    - echo This is my build step
  
Test my demo:
  stage: build
  script:
    - sleep 20s
    - echo This is my test step
  except:
    - master
    
Deploy my demo:
  stage: deploy
  script:
    - sleep 5s
    -  echo This is my deploy step
  only:
    - master
```

* [ ] Check your projects **CI/CD** section for running pipelines. What stages had been executed?

## Task: Create Merge Request
* [ ] In your projects **Merge Request** section create a merge request for this branch on to your *develop*
* [ ] Execute merge when pipeline succeeds
* [ ] Check pipeline that was trigged by the merge in *develop* branch
* [ ] Create merge request from *develop* on to *master* branch
* [ ] Check pipeline that was triggered by the merge in *master* branch
